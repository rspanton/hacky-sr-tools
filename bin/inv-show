#!/bin/sh
if [ -z "$1" ]
then
	echo "Usage: sr `basename \"$0\"` PART_ID"
	exit 1
fi

# Mimic the python implementation of gettoplevel
# Figures out if we're in an inventory clone or not
getTopLeve()
{
	top=`git rev-parse --show-toplevel 2>/dev/null`
	if [ $? -ne 0 ]
	then
		return 1
	fi

	usersFile="$top/.meta/users"
	if [ ! -f "$usersFile" ]
	then
		return 1
	fi

	echo $top
	return 0
}

top=$(getTopLeve)
if [ $? -ne 0 ]
then
	echo "This command must be run in the inventory git repository."
	exit 4
fi

fname=`sr inv-findpart $1`

if [ $? -ne 0 ]
then
	exit 2
fi

git_log_colour()
{
	ui=$(git config --get color.ui)
	if [ -n "$ui"  -a  "$ui" != "false"  ]
	then
		echo "--color"
	else
		diff=$(git config --get color.diff)
		if [ -n "$diff" -a "$diff" != "false" ]
		then
			echo "--color"
		fi
	fi
}

details()
{
	echo "Full name: $fname"
	if [ -d "$fname" ]	# assemblies are directories
	then
		cat "$fname"/info
	else
		cat "$fname"
	fi
	colour=$(git_log_colour)
	git --no-pager log $colour --follow -C -M  "$fname"
}

details | less -R -F -X
